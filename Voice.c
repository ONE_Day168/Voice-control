#include <REGX52.H>
#include "Avoid.h"
#include "Tracking.h"
#include "Follow.h"
#include "OLED.h"
#include "Motor.h"

sbit A25 = P1^5;
sbit A26 = P1^6;
sbit A27 = P1^7;

/**
  * @brief 语音识别，根据A25,A26,A27端口状态启用不同模式
  * @param 无
  * @retval无
  */
void Voice()
{
	//循迹模式
	if(A25 == 0 && A26 == 1 && A27 == 1)
	{
		TrackIng();
	}
	//避障模式
	if(A25 == 1 && A26 == 0 && A27 == 1)
	{
		Start_Avoid();
	}
	//跟随模式
	if(A25 == 1 && A26 == 1 && A27 == 0)
	{
		Follow();
	}
	//停止
	if(A25 == 1 && A26 == 1 && A27 == 1)
	{
		Stop();
	}
	//前进
	if(A25 == 0 && A26 == 0 && A27 == 0)
	{
		GoForward();
	}
	//后退
	if(A25 == 0 && A26 == 1 && A27 == 0)
	{
		GoBack();
	}
	//左转
	if(A25 == 1 && A26 == 0 && A27 == 0)
	{
		GoLeft();
	}
	//右转
	if(A25 == 0 && A26 == 0 && A27 == 1)
	{
		GoRight();
	}
}