#include <REGX52.H>

sbit Sg90_com = P1^0;
unsigned char count,compare;

/**
  * @brief 定时器0初始化函数，舵机PWM控制
  * @param 无
  * @retval无
  */
void Timer0Init(void)		//500微秒@11.0592MHz
{
	TMOD &= 0xF0;		//设置定时器模式
	TMOD |= 0x01;		//设置定时器模式
	TL0 = 0x33;		//设置定时初值
	TH0 = 0xFE;		//设置定时初值
	TF0 = 0;		//清除TF0标志
	TR0 = 1;		//定时器0开始计时
	ET0 = 1;
	EA = 1;
}

//180度
void SgLeft()
{
	compare = 5;
	count = 0;
}

//135度
void SgLeft135()
{
	compare = 4;
	count = 0;
}

//90度
void SgMiddle()
{
	compare = 3;
	count = 0;
}

//45度
void SgRight45()
{
	compare = 2;
	count = 0;
}

//0度
void SgRight()
{
	compare = 1;
	count = 0;
}

//中断处理函数
void Timer0_Rountine() interrupt 1	//每次定时器溢出时是0.5ms
{
	TL0 = 0x33;
	TH0 = 0xFE;
	count++;
	//PWM控制
	if(count < compare)		//通过比较值控制高电平占据周期的时间，也就是占空比大小
	{
		Sg90_com = 1;
	}
	else
	{
		Sg90_com = 0;
	}
	if(count == 40)	//每一个0.5mscount都会++，加了40次就20ms，是舵机控制的一个周期
	{
		count = 0;
		Sg90_com = 1;
	}
}

