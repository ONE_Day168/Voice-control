#include <REGX52.H>
#include "Motor.h"

//跟随模式

//左右红外传感器的信号引脚
sbit FollowLeftSersor = P2^5;
sbit FollowRightSersor = P2^4;

/**
  * @brief 跟随函数，根据左右传感器高低电平判断方向
  * @param 无
  * @retval无
  */
void Follow()
{
	//两个都反射，都亮灯
	if(FollowLeftSersor == 0 && FollowRightSersor == 0)
	{
		GoForward();
	}
	//左边反射，右边没反射，左亮，说明右边没物体跟随了，要左转
	if(FollowLeftSersor == 0 && FollowRightSersor == 1)
	{
		GoLeft();
	}
	//右边反射，左边没反射，右亮，说明左边没物体跟随了，要右转
	if(FollowLeftSersor == 1 && FollowRightSersor == 0)
	{
		GoRight();
	}
	//两个都不反射，都灭灯
	if(FollowLeftSersor == 1 && FollowRightSersor == 1)
	{
		Stop();
	}
}
