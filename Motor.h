#ifndef __Motor_H__
#define __Motor_H__

void GoBack();
void GoForward();
void GoLeft();
void GoLeft_Avoid();
void GoRight();
void GoRight_Avoid();
void Stop();

#endif