#include <REGX52.H>
#include "Motor.h"
#include "Delay.h"
#include "Sg90.h"
#include "Avoid.h"
#include "HC-SR04.h"
#include "OLED.h"
#include "Voice.h"

/*1.根据测距摇头程序进行修改
  2.添加Tracking.c循迹模块
  3.添加Follow.c跟随模块
  4.新增Voice.c语音识别模块，根据SU-03T输出引脚的不同高低电平执行不同的任务
  5.OLED屏可根据需要接电路显示，不过在OLED显示时会去遍历点阵数据，有for循环，
  清屏函数也有，如果for循环调用得多，对循迹和跟随模块的实时性会造成影响，
  有可能模块的输出脚发生电平转换，但程序还在for循环里跑，就不能及时改变状态
*/

void main()
{
	Timer0Init();		//Sg90
	Timer2Init();		//HC-SR04
	HC_Init();
	while(1)
	{
		//语音识别
		Voice();
	}
}

