#include <REGX52.H>

//右轮
sbit rightA = P3^7;
sbit rightB = P3^3;
//左轮
sbit leftA = P3^4;
sbit leftB = P3^5;

/**
  * @brief控制小车后退
  * @param无
  * @retval无
  */
void GoBack()
{
	rightA = 1;
	rightB = 0;
	
	leftA = 1;
	leftB = 0;
}
/**
  * @brief控制小车前进
  * @param无
  * @retval无
  */
void GoForward()
{
	rightA = 0;
	rightB = 1;
	
	leftA = 0;
	leftB = 1;
}

/**
  * @brief控制小车左转
  * @param
  * @retval
  */
void GoLeft()
{
	//右轮前转
	rightA = 0;
	rightB = 1;
	//左轮不动
	leftA =0;
	leftB = 0;
}


/**
  * @brief控制小车右转
  * @param无
  * @retval无
  */
void GoRight()
{
	//右轮不动
	rightA = 0;
	rightB = 0;
	//左轮前转
	leftA = 0;
	leftB = 1;
}

/**
  * @brief控制小车左转-避障模式使用
  * @param无
  * @retval无
  */
void GoLeft_Avoid()
{
	//右轮前转
	rightA = 0;
	rightB = 1;
	//左轮后退
	leftA = 1;
	leftB = 0;
}

/**
  * @brief控制小车右转-避障模式使用
  * @param无
  * @retval无
  */
void GoRight_Avoid()
{
	//右轮后退
	rightA = 1;
	rightB = 0;
	//左轮前转
	leftA = 0;
	leftB = 1;
}

/**
  * @brief控制小车停止
  * @param无
  * @retval无
  */

void Stop()
{
	//右轮不动
	rightA = 0;
	rightB = 0;
	//左轮不动
	leftA = 0;
	leftB = 0;
}