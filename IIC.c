#include <REGX52.H>

sbit SCL = P0^1;
sbit SDA = P0^3;

/**
  * @brief IIC起始信号
  * @param 无
  * @retval无
  */
void IIC_Start()
{	
	SCL = 1;
	SDA = 1;
	SDA = 0;
	SCL = 0;
}	

/**
  * @brief IIC停止信号
  * @param 无
  * @retval无
  */
void IIC_Stop()
{
	SDA = 0;
	SCL = 1;
	SDA = 1;
}

/**
  * @brief IIC发送一个字节
  * @param 待发送的字节数据
  * @retval无
  */
void IIC_SendByte(unsigned char dat)
{
	unsigned int i;
	for(i = 0 ; i < 8; i++)
	{
		SDA = dat & (0x80 >> i);
		SCL = 1;
		SCL = 0;
	}
}

/**
  * @brief IIC接收应答
  * @param 无
  * @retval返回应答
  */
unsigned char IIC_RecACK()
{
	unsigned char ack;
	SDA = 1;	//主机释放数据线
	SCL = 1;
	ack = SDA;
	SCL = 0;
	return ack;
}
