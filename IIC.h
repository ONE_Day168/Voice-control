#ifndef __IIC_H__
#define __IIC_H__

void IIC_Start();
void IIC_Stop();
void IIC_SendByte(unsigned char dat);
unsigned char IIC_RecACK();

#endif