#ifndef __OLED_H__
#define __OLED_H__

void OLED_WritCommand(unsigned char Command);
void OLED_WritData(unsigned char Data);
void OLED_Init();
void OLED_Clear();
void Show_Data(int num,char* Data);

void OLED_Set_Pos(unsigned char x, unsigned char y);
void OLED_P8x16Str(unsigned char x, y,unsigned char ch[]);

void Show_xunji();

#endif