#include <REGX52.H>
#include "IIC.h"
#include "OLEDASCII.h"

/**
  * @brief OLED写命令
  * @param 要写入的命令
  * @retval无
  */
void OLED_WritCommand(unsigned char Command)
{
	IIC_Start();
	IIC_SendByte(0x78);	//从机地址+读取
	IIC_RecACK();
	IIC_SendByte(0x00);	//设置为写入指令
	IIC_RecACK();
	IIC_SendByte(Command);	//写入指令
	IIC_RecACK();
	IIC_Stop();
}


/**
  * @brief OLED写数据
  * @param 要写入的数据
  * @retval无
  */
void OLED_WritData(unsigned char Data)
{
	IIC_Start();
	IIC_SendByte(0x78);	//从机地址+读取
	IIC_RecACK();
	IIC_SendByte(0x40);	//设置为写入数据
	IIC_RecACK();
	IIC_SendByte(Data);	//写入指令
	IIC_RecACK();
	IIC_Stop();
}

/**
  * @brief OLED清屏函数
  * @param 无
  * @retval无
  */
void OLED_Clear()
{
	unsigned int i,j;
	//先找出PAGE0~PAGE7
	for(i = 0;i<8;i++)
	{
		OLED_WritCommand(0xB0+i);
		//从第0列开始
		OLED_WritCommand(0x00);
		OLED_WritCommand(0x10);
		//0到127列，依次写入0，每写入数据，列地址自动偏移
		for(j = 0;j<128;j++)
		{
			OLED_WritData(0);
		}
	}
}

/**
  * @brief OLED初始化，数据手册已给出
  * @param 无
  * @retval无
  */
void OLED_Init()
{
	OLED_WritCommand(0xAE);
	OLED_WritCommand(0x00);
	OLED_WritCommand(0x10);
	OLED_WritCommand(0x40);
	OLED_WritCommand(0xB0);
	OLED_WritCommand(0x81);
	OLED_WritCommand(0xFF);
	OLED_WritCommand(0xA1);
	OLED_WritCommand(0xA6);
	OLED_WritCommand(0xA8);
	OLED_WritCommand(0x3F);
	OLED_WritCommand(0xC8);
	OLED_WritCommand(0xD3);
	OLED_WritCommand(0x00);
	OLED_WritCommand(0xD5);
	OLED_WritCommand(0x80);
	OLED_WritCommand(0xD8);
	OLED_WritCommand(0x05);
	OLED_WritCommand(0xD9);
	OLED_WritCommand(0xF1);
	OLED_WritCommand(0xDA);
	OLED_WritCommand(0x12);
	OLED_WritCommand(0xDB);
	OLED_WritCommand(0x30);
	OLED_WritCommand(0x8D);
	OLED_WritCommand(0x14);
	OLED_WritCommand(0xAF);
}

/**
  * @brief 显示文字，字母或数字
  * @param num 循环次数，对应文字字母或数字的宽度
  * @param Data 要显示的文字，字母或数字的数组
  * @retval无
  */
void Show_Data(int num,char* Data)
{
	unsigned int i;
	for(i = 0;i<num;i++)
	{
		OLED_WritData(Data[i]);	
	}
}



/*********************OLED 设置坐标************************************/
void OLED_Set_Pos(unsigned char x, unsigned char y) 
{ 
	OLED_WritCommand(0xb0+y);
	OLED_WritCommand(((x&0xf0)>>4)|0x10);
	OLED_WritCommand((x&0x0f)|0x01);
} 


/*******************功能描述：显示8*16一组标准ASCII字符串	 显示的坐标（x,y），y为页范围0～7
****************/
void OLED_P8x16Str(unsigned char x, y,unsigned char ch[])
{
	unsigned char c=0,i=0,j=0;
	while (ch[j]!='\0')
	{
		c =ch[j]-32;
		if(x>120){x=0;y++;}
		OLED_Set_Pos(x,y);
		for(i=0;i<8;i++)
		OLED_WritData(F8X16[c*16+i]);
		OLED_Set_Pos(x,y+1);
		for(i=0;i<8;i++)
		OLED_WritData(F8X16[c*16+i+8]);
		x+=8;
		j++;
	}
}

/**
  * @brief OLED显示“循迹模式”
  * @param 无
  * @retval无
  */
void Show_xunji()
{
	//1.确认页寻址模式
	OLED_WritCommand(0x20);	//设置内存地址
	OLED_WritCommand(0x02);	//设置页寻址模式
	//选择PAGE0
	OLED_WritCommand(0xB3);
	//选择列
	OLED_WritCommand(0x00);
	OLED_WritCommand(0x10);
	//显示上半部分
	Show_Data(16,xun1);
	Show_Data(16,ji1);
	Show_Data(16,mo1);
	Show_Data(16,shi1);
	
	//选择Page1
	OLED_WritCommand(0xB4);
	//选择列，要跟上半部分的列一致
	OLED_WritCommand(0x00);
	OLED_WritCommand(0x10);
	//显示下半部分
	Show_Data(16,xun2);
	Show_Data(16,ji2);
	Show_Data(16,mo2);
	Show_Data(16,shi2);
}

