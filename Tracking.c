#include <REGX52.H>
#include "Motor.h"

//循迹模式

//左右红外传感器的信号引脚
sbit TrackIngLeftSersor = P2^7;
sbit TrackIngRightSersor = P2^6;

/**
  * @brief 循迹函数，根据左右传感器高低电平判断线路
  * @param 无
  * @retval无
  */
void TrackIng()
{
	//两个都反射，都亮灯
	if(TrackIngLeftSersor == 0 && TrackIngRightSersor == 0)
	{
		GoForward();
	}
	//左边反射，右边没反射，左亮，说明右边碰到黑线红外线被吸收，要右转
	if(TrackIngLeftSersor == 0 && TrackIngRightSersor == 1)
	{
		GoRight();
	}
	//右边反射，左边没反射，右亮，说明左边碰到黑线红外线被吸收，要左转
	if(TrackIngLeftSersor == 1 && TrackIngRightSersor == 0)
	{
		GoLeft();
	}
	//两个都不反射，都灭灯
	if(TrackIngLeftSersor == 1 && TrackIngRightSersor == 1)
	{
		Stop();
	}
}