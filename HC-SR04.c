#include <REGX52.H>
#include "Delay.h"

sbit Trig = P1^1;
sbit Echo = P1^2;

/**
  * @brief定时器2初始化，超声波用
  * @param无
  * @retval无
  */
void Timer2Init(void)		//500微秒@11.0592MHz
{
	T2MOD = 0;		//初始化模式寄存器
	T2CON = 0;		//初始化控制寄存器
	TL2 = 0;		//设置定时初值
	TH2 = 0;		//设置定时初值
	RCAP2L = 0;		//设置定时重载值
	RCAP2H = 0;		//设置定时重载值
	TR2 = 0;		//定时器2不开始计时
}


//起始信号
void Trig_Start()
{
	Trig = 0;
	Trig = 1;
	Delay11us();
	Trig = 0;
}

/**
  * @brief超声波测距
  * @param无
  * @retval返回测到的距离，单位cm
  */
double ultrasonic()
{
	double time;
	double distance;
	TH2 = 0;
	TL2 = 0;
	Trig_Start();
	while(Echo == 0);
	TR2 = 1;
	while(Echo == 1);
	TR2 = 0;
	time = (TH2*256+TL2)*1.085;
	distance = time*0.017;
	return distance;
}