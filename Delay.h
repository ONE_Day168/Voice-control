#ifndef __Delay_H__
#define __Delay_H__

void Delay1ms(unsigned int num);
void Delay10us();
void Delay11us();
#endif