#include "intrins.h"
/**
  * @brief延时函数
  * @param延时时间，单位ms
  * @retval无
  */
void Delay1ms(unsigned int num)		//@11.0592MHz
{
	unsigned char i, j;

	while(num--)
	{
		i = 2;
		j = 199;
		do
		{
			while (--j);
		} while (--i);
	}
}


/**
  * @brief延时函数
  * @param延时时间，单位ms
  * @retval无
  */
void Delay10us()		//@11.0592MHz
{
	unsigned char i;

	i = 2;
	while (--i);
}

void Delay11us()		//@11.0592MHz
{
	unsigned char i;

	_nop_();
	i = 2;
	while (--i);
}

